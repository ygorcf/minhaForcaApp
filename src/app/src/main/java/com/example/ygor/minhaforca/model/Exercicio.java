package com.example.ygor.minhaforca.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ygor on 09/02/2018.
 */

public class Exercicio extends GenericModel {

    private String nome;
    private List<ExercicioSerie> series;
    private String observacoes;
    private Exercicio proxExercicio;

    public List<Exercicio> getListExercicios() {
        List<Exercicio> ret = new ArrayList<>();
        Exercicio e = this;

        while (e != null) {
            ret.add(e);
            e = e.getProxExercicio();
        }

        return ret;
    }

    public Exercicio() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<ExercicioSerie> getSeries() {
        return series;
    }

    public void setSeries(List<ExercicioSerie> series) {
        this.series = series;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public Exercicio getProxExercicio() {
        return proxExercicio;
    }

    public void setProxExercicio(Exercicio proxExercicio) {
        this.proxExercicio = proxExercicio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Exercicio exercicio = (Exercicio) o;

        if (nome != null ? !nome.equals(exercicio.nome) : exercicio.nome != null) return false;
        if (series != null ? !series.equals(exercicio.series) : exercicio.series != null)
            return false;
        if (observacoes != null ? !observacoes.equals(exercicio.observacoes) : exercicio.observacoes != null)
            return false;
        return proxExercicio != null ? proxExercicio.equals(exercicio.proxExercicio) : exercicio.proxExercicio == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (nome != null ? nome.hashCode() : 0);
        result = 31 * result + (series != null ? series.hashCode() : 0);
        result = 31 * result + (observacoes != null ? observacoes.hashCode() : 0);
        result = 31 * result + (proxExercicio != null ? proxExercicio.hashCode() : 0);
        return result;
    }
}
