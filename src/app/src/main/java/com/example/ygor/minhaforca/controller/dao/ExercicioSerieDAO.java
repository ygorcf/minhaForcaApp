package com.example.ygor.minhaforca.controller.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.ygor.minhaforca.model.Exercicio;
import com.example.ygor.minhaforca.model.ExercicioSerie;
import com.example.ygor.minhaforca.model.Serie;

import java.util.List;

/**
 * Created by Ygor on 13/02/2018.
 */

public class ExercicioSerieDAO extends GenericDAO<ExercicioSerie> implements GenericDAO.TransformCursorListener<ExercicioSerie> {

    public static final String NOME_TABELA = "exercicio_serie";

    private static final String COL_ID_EXERCICIO = "_id_exercicio";
    private static final String COL_ID_SERIE = "_id_serie";
    private static final String COL_QUANTIDADE = "quantidade";

    public static final String SQL_TABELA = "CREATE TABLE " + NOME_TABELA + "(" +
            COL_ID_EXERCICIO + " INTEGER, " +
            COL_ID_SERIE + " INTEGER, " +
            COL_QUANTIDADE + " INT, " +
            "FOREIGN KEY(" + COL_ID_EXERCICIO + ") REFERENCES " + ExercicioDAO.NOME_TABELA + "(" + ExercicioDAO.COL_ID + ") ON UPDATE CASCADE ON DELETE RESTRICT, " +
            "FOREIGN KEY(" + COL_ID_SERIE + ") REFERENCES " + SerieDAO.NOME_TABELA + "(" + SerieDAO.COL_ID + ") ON UPDATE CASCADE ON DELETE RESTRICT);";

    public ExercicioSerieDAO(SQLiteOpenHelper helper) {
        super(helper, NOME_TABELA);
    }

    @Override
    public long salvar(ExercicioSerie exercicioSerie) {
        ContentValues c = new ContentValues();
        c.put(COL_ID_EXERCICIO, exercicioSerie.getExercicio().getId());
        c.put(COL_ID_EXERCICIO, exercicioSerie.getSerie().getId());
        c.put(COL_QUANTIDADE, exercicioSerie.getQuantidade());
        return this.salvar(c);
    }

    @Override
    public long atualizar(ExercicioSerie exercicioSerie) {
        ContentValues c = new ContentValues();
        c.put(COL_ID_EXERCICIO, exercicioSerie.getExercicio().getId());
        c.put(COL_ID_EXERCICIO, exercicioSerie.getSerie().getId());
        c.put(COL_QUANTIDADE, exercicioSerie.getQuantidade());
        return this.atualizar(COL_ID_EXERCICIO + "=? AND " + COL_ID_SERIE + "=?",
                new String[]{String.valueOf(exercicioSerie.getExercicio().getId()),
                                String.valueOf(exercicioSerie.getSerie().getId())},
                c);
    }

    @Override
    public long deletar(long id) {
        return -1;
    }

    public long deletar(long idExercicio, long idSerie) {
        return this.deletar(COL_ID_EXERCICIO + "=? AND " + COL_ID_SERIE + "=?",
                new String[]{String.valueOf(idSerie),
                        String.valueOf(idExercicio)});
    }

    @Override
    public ExercicioSerie pesquisar(long id) {
        return null;
    }

    public ExercicioSerie pesquisar(long idExercicio, long idSerie) {
        return this.pesquisar(COL_ID_EXERCICIO + "=? AND " + COL_ID_SERIE + "=?",
                new String[]{String.valueOf(idSerie),
                        String.valueOf(idExercicio)}, this);
    }

    @Override
    public List<ExercicioSerie> listar() {
        return this.listar(this);
    }

    public List<ExercicioSerie> listarPeloExercicio(final Exercicio exercicio) {
        return this.listar(COL_ID_EXERCICIO + "=?", new String[]{String.valueOf(exercicio.getId())}, new TransformCursorListener<ExercicioSerie>() {
            @Override
            public ExercicioSerie getFromCursor(Cursor cursor) {
                SerieDAO sBD = new SerieDAO(helper);
                return getFromCursorGeneric(cursor,
                        exercicio,
                        sBD.pesquisar(cursor.getLong(cursor.getColumnIndex(COL_ID_SERIE))));
            }
        });
    }

    public List<ExercicioSerie> listarPelaSerie(final Serie serie) {
        return this.listar(COL_ID_SERIE + "=?", new String[]{String.valueOf(serie.getId())}, new TransformCursorListener<ExercicioSerie>() {
            @Override
            public ExercicioSerie getFromCursor(Cursor cursor) {
                ExercicioDAO eBD = new ExercicioDAO(helper);
                return getFromCursorGeneric(cursor,
                        eBD.pesquisar(cursor.getLong(cursor.getColumnIndex(COL_ID_EXERCICIO))),
                        serie);
            }
        });
    }

    private ExercicioSerie getFromCursorGeneric(Cursor cursor, Exercicio exercicio, Serie serie) {
        ExercicioSerie exercicioSerie = new ExercicioSerie();

        exercicioSerie.setExercicio(exercicio);
        exercicioSerie.setSerie(serie);
        exercicioSerie.setQuantidade(cursor.getInt(cursor.getColumnIndex(COL_QUANTIDADE)));

        return exercicioSerie;
    }

    @Override
    public ExercicioSerie getFromCursor(Cursor cursor) {
        ExercicioDAO eBD = new ExercicioDAO(helper);
        SerieDAO sBD = new SerieDAO(helper);
        return getFromCursorGeneric(cursor,
                eBD.pesquisar(cursor.getLong(cursor.getColumnIndex(COL_ID_EXERCICIO))),
                sBD.pesquisar(cursor.getLong(cursor.getColumnIndex(COL_ID_SERIE))));
    }
}
