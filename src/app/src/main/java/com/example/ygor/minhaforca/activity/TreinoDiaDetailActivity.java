package com.example.ygor.minhaforca.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ygor.minhaforca.R;
import com.example.ygor.minhaforca.adapter.ExerciciosAdapter;
import com.example.ygor.minhaforca.controller.dao.ExercicioDAO;
import com.example.ygor.minhaforca.helper.DBHelper;
import com.example.ygor.minhaforca.model.Exercicio;
import com.example.ygor.minhaforca.model.TreinoDia;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TreinoDiaDetailActivity extends AppCompatActivity implements ExerciciosAdapter.OnItemExercicioClickListener {

    private final int CONTEXT_ALTERAR = 0;
    private final int CONTEXT_DELETAR = 1;

    private TreinoDia treinoDia;
    private DBHelper helper;
    private List<Exercicio> exercicios;
    private ExerciciosAdapter adapter;

    @BindView(R.id.listaExercicios)
    ListView listaExercicios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_treino_dia_detail);
        ButterKnife.bind(this);

        treinoDia = (TreinoDia) getIntent().getSerializableExtra("treinoDia");

        helper = new DBHelper(this);
        exercicios = getExerciciosList();

        adapter = new ExerciciosAdapter(this, exercicios, this);

        listaExercicios.setAdapter(adapter);
        listaExercicios.setDivider(null);
        listaExercicios.setDividerHeight(0);
        registerForContextMenu(listaExercicios);

    }

    private void openManterExercicio(Exercicio exercicio) {
        Intent i = new Intent(this, ManterActivity.class);
        i.putExtra("model", exercicio);
        startActivityForResult(i, 100);
    }

    private void openDetail(Exercicio exercicio) {
//        Intent i = new Intent(this, );
//        i.putExtra("exercicio", exercicio);
//        startActivityForResult(i, 100);
    }

    private long deletarExercicio(Exercicio exercicio) {
        return new ExercicioDAO(helper).deletar(exercicio.getId());
    }

    private void updateList(List<Exercicio> exerciciosNovos) {
        exercicios.clear();
        exercicios.addAll(exerciciosNovos);
        adapter.notifyDataSetChanged();
    }

    private List<Exercicio> getExerciciosList() {
//        return new ExercicioDAO(helper).listarPeloTreinoDia(treinoDia);
        return new ExercicioDAO(helper).listar();
    }

    @OnClick(R.id.btnAdd)
    public void onBtnAddClick() {
        openManterExercicio(new Exercicio());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        updateList(getExerciciosList());
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Exercicio exercicio = (Exercicio) adapter.getItem(info.position);

        switch (item.getItemId()) {
            case CONTEXT_ALTERAR:
                openManterExercicio(exercicio);
                break;
            case CONTEXT_DELETAR:
                if (deletarExercicio(exercicio) > 0) {
                    updateList(getExerciciosList());
                }
                break;
            default:
                Toast.makeText(this, R.string.labelTreinoDiaDetailOpcaoInvalida, Toast.LENGTH_SHORT).show();
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(menu.NONE, CONTEXT_ALTERAR, menu.NONE, R.string.labelTreinoDiaDetailAlterar);
        menu.add(menu.NONE, CONTEXT_DELETAR, menu.NONE, R.string.labelTreinoDiaDetailDeletar);
    }

    @Override
    public void onItemExercicioClick(Exercicio exercicio) {
        openDetail(exercicio);
    }
}
