package com.example.ygor.minhaforca.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.example.ygor.minhaforca.R;
import com.example.ygor.minhaforca.adapter.OpcoesAdapter;
import com.example.ygor.minhaforca.helper.DBHelper;
import com.example.ygor.minhaforca.helper.DBPopulator;
import com.example.ygor.minhaforca.model.OpcaoMain;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.listaAcoes)
    ListView listaAcoes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        DBPopulator.deleteAll(new DBHelper(this).getWritableDatabase());
        DBPopulator.populate(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        listaAcoes.setDivider(null);
        listaAcoes.setDividerHeight(0);

        List<OpcaoMain> opcoes = new ArrayList<>();
        opcoes.add(new OpcaoMain("Treinos", TreinosActivity.class));
        opcoes.add(new OpcaoMain("Opções", OpcoesActivity.class));

        OpcoesAdapter adapter = new OpcoesAdapter(this, opcoes);

        listaAcoes.setAdapter(adapter);
    }
}
