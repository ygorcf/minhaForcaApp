package com.example.ygor.minhaforca.controller.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.ygor.minhaforca.model.Treino;

import java.util.List;

/**
 * Created by Ygor on 16/02/2018.
 */

public class TreinoDAO extends GenericDAO<Treino> implements GenericDAO.TransformCursorListener<Treino> {

    public static final String NOME_TABELA = "treino";

    public static final String COL_ID = "_id";
    private static final String COL_NOME = "nome";
    private static final String COL_DURACAO = "duracao";
    private static final String COL_OBJETIVO = "objetivo";

    public static final String SQL_TABELA = "CREATE TABLE " + NOME_TABELA + "(" +
            COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COL_NOME + " TEXT, " +
            COL_DURACAO + " INT," +
            COL_OBJETIVO + " TEXT);";

    public TreinoDAO(SQLiteOpenHelper helper) {
        super(helper, NOME_TABELA);
    }

    @Override
    public long salvar(Treino treino) {
        ContentValues c = new ContentValues();
        c.put(COL_NOME, treino.getNome());
        c.put(COL_DURACAO, treino.getDuracao());
        c.put(COL_OBJETIVO, treino.getObjetivo());
        return salvar(c);
    }

    @Override
    public long atualizar(Treino treino) {
        ContentValues c = new ContentValues();
        c.put(COL_NOME, treino.getNome());
        c.put(COL_DURACAO, treino.getDuracao());
        c.put(COL_OBJETIVO, treino.getObjetivo());
        return atualizar(COL_ID + "=?",
                            new String[]{String.valueOf(treino.getId())},
                            c);
    }

    @Override
    public long deletar(long id) {
        return deletar(COL_ID + "=?",
                new String[]{String.valueOf(id)});
    }

    @Override
    public Treino pesquisar(long id) {
        return pesquisar(COL_ID + "=?",
                new String[]{String.valueOf(id)},
                this);
    }

    @Override
    public List<Treino> listar() {
        return listar(this);
    }

    @Override
    public Treino getFromCursor(Cursor cursor) {
        Treino t = new Treino();
        t.setId(cursor.getLong(cursor.getColumnIndex(COL_ID)));
        t.setNome(cursor.getString(cursor.getColumnIndex(COL_NOME)));
        t.setDuracao(cursor.getLong(cursor.getColumnIndex(COL_DURACAO)));
        t.setObjetivo(cursor.getString(cursor.getColumnIndex(COL_OBJETIVO)));
        return t;
    }
}
