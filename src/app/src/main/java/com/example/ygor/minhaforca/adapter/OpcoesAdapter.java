package com.example.ygor.minhaforca.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ygor.minhaforca.R;
import com.example.ygor.minhaforca.model.OpcaoMain;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ygor on 08/02/2018.
 */

public class OpcoesAdapter extends GenericAdapter<OpcoesAdapter.OpcoesHolder> {

    List<OpcaoMain> opcoes;

    public OpcoesAdapter(Context context, List<OpcaoMain> opcoes) {
        super(context, R.layout.item_main, OpcoesHolder.class);
        this.opcoes = opcoes;
    }

    @Override
    public int getCount() {
        return opcoes.size();
    }

    @Override
    public Object getItem(int position) {
        return opcoes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View inflateView(int position, OpcoesHolder holder, View view, ViewGroup parent) {
        final OpcaoMain opcao = (OpcaoMain) getItem(position);
        holder.tvLabel.setText(opcao.getLabel());
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, opcao.getActivity());
                context.startActivity(i);
            }
        });
        return view;
    }

    class OpcoesHolder extends GenericAdapter.GenericHolder {

        OpcoesHolder (View view) {
            super(view);
        }

        @BindView(R.id.label)
        TextView tvLabel;
    }

}
