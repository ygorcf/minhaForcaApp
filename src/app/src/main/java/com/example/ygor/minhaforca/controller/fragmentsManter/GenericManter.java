package com.example.ygor.minhaforca.controller.fragmentsManter;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.example.ygor.minhaforca.controller.dao.GenericDAO;
import com.example.ygor.minhaforca.fragment.ManterFragment;
import com.example.ygor.minhaforca.model.GenericModel;
import com.example.ygor.minhaforca.model.Treino;

import java.io.Serializable;

/**
 * Created by Ygor on 27/02/2018.
 */

public abstract class GenericManter<T extends GenericModel, K extends GenericDAO<T>> implements ManterFragment.ModelSaver<T, K>, Serializable {

    private FragmentManager manager;

    public GenericManter(FragmentManager manager, int frameLayout, int fragmentLayout, T model, K dao) {
        this.manager = manager;
        manager.beginTransaction()
                .replace(frameLayout, ManterFragment.newInstance(fragmentLayout, model, dao, this))
                .commit();
    }

    public FragmentManager getManager() {
        return manager;
    }

    public void setManager(FragmentManager manager) {
        this.manager = manager;
    }
}
