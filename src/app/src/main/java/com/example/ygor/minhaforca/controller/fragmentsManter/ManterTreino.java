package com.example.ygor.minhaforca.controller.fragmentsManter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.example.ygor.minhaforca.R;
import com.example.ygor.minhaforca.controller.dao.TreinoDAO;
import com.example.ygor.minhaforca.fragment.DatePickerFragment;
import com.example.ygor.minhaforca.fragment.ManterFragment;
import com.example.ygor.minhaforca.helper.DBHelper;
import com.example.ygor.minhaforca.helper.DateHelper;
import com.example.ygor.minhaforca.model.Treino;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ygor on 17/02/2018.
 */

public class ManterTreino extends GenericManter<Treino, TreinoDAO> implements DatePickerDialog.OnDateSetListener {

    private Calendar data;

    @BindView(R.id.btnSave)
    Button btnSave;
    @BindView(R.id.btnDuracao)
    Button btnDuracao;

    @BindView(R.id.labelDuracao)
    TextView tvDuracao;

    @BindView(R.id.campoManterTreinoNome)
    EditText campoNome;

    @BindView(R.id.campoManterTreinoObjetivo)
    EditText campoObjetivo;

    public ManterTreino(Context context, FragmentManager manager, int frameLayout, int fragmentLayout, Treino treino) {
        super(manager, frameLayout, fragmentLayout, treino, new TreinoDAO(new DBHelper(context)));
        data = Calendar.getInstance();
    }

    private void setLabelData() {
        tvDuracao.setText(DateHelper.formatDate(data));
    }

    @Override
    public Button onViewInflate(View fragmentView, Bundle savedInstanceState, Treino treino) {
        ButterKnife.bind(this, fragmentView);

        if (treino.getId() != -1) {
            data.clear();
            data.setTimeInMillis(treino.getDuracao());
            campoNome.setText(treino.getNome());
            campoObjetivo.setText(treino.getObjetivo());
        }
        if (savedInstanceState != null) {
            long dataMillis = savedInstanceState.getLong("data");
            data.clear();
            data.setTimeInMillis(dataMillis);
            campoNome.setText(savedInstanceState.getString("nome"));
            campoObjetivo.setText(savedInstanceState.getString("objetivo"));
        }
        setLabelData();

        return btnSave;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("nome", campoNome.getText().toString());
        outState.putSerializable("data", data.getTime().getTime());
        outState.putString("objetivo", campoObjetivo.getText().toString());
    }

    @Override
    public boolean formIsValid() {
        if (campoNome.length() == 0) {
            return false;
        }
        return true;
    }

    @Override
    public void inflateModel(Treino treino) {
        treino.setNome(campoNome.getText().toString());
        treino.setDuracao(data.getTimeInMillis());
        treino.setObjetivo(campoObjetivo.getText().toString());
    }

    @OnClick(R.id.btnDuracao)
    public void onBtnDuracaoClick() {
        DatePickerFragment fragment = DatePickerFragment.newInstance(this, data);
        fragment.show(this.getManager(), "datePicker");
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        data.clear();
        data.set(year, month, dayOfMonth);
        setLabelData();
    }
}
