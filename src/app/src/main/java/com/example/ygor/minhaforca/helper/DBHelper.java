package com.example.ygor.minhaforca.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.ygor.minhaforca.controller.dao.ExercicioDAO;
import com.example.ygor.minhaforca.controller.dao.ExercicioSerieDAO;
import com.example.ygor.minhaforca.controller.dao.SerieDAO;
import com.example.ygor.minhaforca.controller.dao.TreinoDAO;
import com.example.ygor.minhaforca.controller.dao.TreinoDiaDAO;

import java.util.List;

/**
 * Created by Ygor on 09/02/2018.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final int versaoDB = 2;
    private static final String nomeDB = "minhaForca.db";

    public DBHelper(Context context) {
        super(context, nomeDB, null, versaoDB);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SerieDAO.SQL_TABELA);
        db.execSQL(ExercicioDAO.SQL_TABELA);
        db.execSQL(ExercicioSerieDAO.SQL_TABELA);
        db.execSQL(TreinoDiaDAO.SQL_TABELA);
        db.execSQL(TreinoDAO.SQL_TABELA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        DBPopulator.deleteAll(db);
        db.execSQL("DROP TABLE " + SerieDAO.NOME_TABELA + ";");
        db.execSQL("DROP TABLE " + ExercicioDAO.NOME_TABELA + ";");
        db.execSQL("DROP TABLE " + ExercicioSerieDAO.NOME_TABELA + ";");
        db.execSQL("DROP TABLE " + TreinoDiaDAO.NOME_TABELA + ";");
        db.execSQL("DROP TABLE " + TreinoDAO.NOME_TABELA + ";");
        onCreate(db);
    }

}
