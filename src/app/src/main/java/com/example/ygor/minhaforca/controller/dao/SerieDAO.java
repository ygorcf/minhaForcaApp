package com.example.ygor.minhaforca.controller.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.ygor.minhaforca.model.Serie;

import java.util.List;

/**
 * Created by Ygor on 10/02/2018.
 */

public class SerieDAO extends GenericDAO<Serie> implements GenericDAO.TransformCursorListener<Serie> {

    public static final String NOME_TABELA = "serie";

    public static final String COL_ID = "_id";
    private static final String COL_REPETICOES = "repeticoes";
    private static final String COL_SEG_ANTES = "seg_antes";
    private static final String COL_SEG_DEPOIS = "seg_depois";
    private static final String COL_CADENCIA_0 = "cadencia0";
    private static final String COL_CADENCIA_1 = "cadencia1";
    private static final String COL_PICO_CONTRACAO = "pico_contracao";
    private static final String COL_TEMPOS = "tempos";

    public static final String SQL_TABELA = "CREATE TABLE " + NOME_TABELA + "(" +
                                                COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                                COL_REPETICOES + " INT, " +
                                                COL_SEG_ANTES + " INT, " +
                                                COL_SEG_DEPOIS + " INT, " +
                                                COL_CADENCIA_0 + " INT, " +
                                                COL_CADENCIA_1 + " INT, " +
                                                COL_PICO_CONTRACAO + " INT, " +
                                                COL_TEMPOS + " INT);";

    public SerieDAO(SQLiteOpenHelper helper) {
        super(helper, NOME_TABELA);
    }

    @Override
    public long salvar(Serie serie) {
        ContentValues values = new ContentValues();
        values.put(COL_REPETICOES, serie.getRepeticoes());
        values.put(COL_SEG_ANTES, serie.getSegAntes());
        values.put(COL_SEG_DEPOIS, serie.getSegDepois());
        values.put(COL_CADENCIA_0, serie.getCadencia()[0]);
        values.put(COL_CADENCIA_1, serie.getCadencia()[1]);
        values.put(COL_PICO_CONTRACAO, serie.isPicoContracao());
        values.put(COL_TEMPOS, serie.getTempos());
        return this.salvar(values);
    }

    @Override
    public long atualizar(Serie serie) {
        ContentValues values = new ContentValues();
        values.put(COL_REPETICOES, serie.getRepeticoes());
        values.put(COL_SEG_ANTES, serie.getSegAntes());
        values.put(COL_SEG_DEPOIS, serie.getSegDepois());
        values.put(COL_CADENCIA_0, serie.getCadencia()[0]);
        values.put(COL_CADENCIA_1, serie.getCadencia()[1]);
        values.put(COL_PICO_CONTRACAO, serie.isPicoContracao());
        values.put(COL_TEMPOS, serie.getTempos());
        return this.atualizar(COL_ID + "=?", new String[]{String.valueOf(serie.getId())}, values);
    }

    @Override
    public long deletar(long id) {
        return this.deletar(COL_ID + "=?", new String[]{String.valueOf(id)});
    }

    @Override
    public Serie pesquisar(long id) {
        return this.pesquisar(COL_ID + "=?", new String[]{String.valueOf(id)}, this);
    }

    @Override
    public List<Serie> listar() {
        return listar(this);
    }

    @Override
    public Serie getFromCursor(Cursor cursor) {
        Serie s = new Serie();

        s.setId(cursor.getLong(cursor.getColumnIndex(COL_ID)));
        s.setRepeticoes(cursor.getInt(cursor.getColumnIndex(COL_REPETICOES)));
        s.setSegAntes(cursor.getInt(cursor.getColumnIndex(COL_SEG_ANTES)));
        s.setSegDepois(cursor.getInt(cursor.getColumnIndex(COL_SEG_DEPOIS)));
        s.setCadencia(new long[]{cursor.getLong(cursor.getColumnIndex(COL_CADENCIA_0)), cursor.getLong(cursor.getColumnIndex(COL_CADENCIA_1))});
        s.setPicoContracao(cursor.getInt(cursor.getColumnIndex(COL_PICO_CONTRACAO)) == 1);
        s.setTempos(cursor.getInt(cursor.getColumnIndex(COL_TEMPOS)));

        return s;
    }
}
