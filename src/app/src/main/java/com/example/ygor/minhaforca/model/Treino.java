package com.example.ygor.minhaforca.model;

import java.util.List;

/**
 * Created by Ygor on 09/02/2018.
 */

public class Treino extends GenericModel {

    private String nome;
    private long duracao;
    private String objetivo;
    private List<TreinoDia> treinoDias;

    public Treino() {
    }

    public Treino(String nome, long duracao, String objetivo, List<TreinoDia> treinoDias) {
        this.nome = nome;
        this.duracao = duracao;
        this.objetivo = objetivo;
        this.treinoDias = treinoDias;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getDuracao() {
        return duracao;
    }

    public void setDuracao(long duracao) {
        this.duracao = duracao;
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

    public List<TreinoDia> getTreinoDias() {
        return treinoDias;
    }

    public void setTreinoDias(List<TreinoDia> treinoDias) {
        this.treinoDias = treinoDias;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Treino treino = (Treino) o;

        if (duracao != treino.duracao) return false;
        if (nome != null ? !nome.equals(treino.nome) : treino.nome != null) return false;
        if (objetivo != null ? !objetivo.equals(treino.objetivo) : treino.objetivo != null)
            return false;
        return treinoDias != null ? treinoDias.equals(treino.treinoDias) : treino.treinoDias == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (nome != null ? nome.hashCode() : 0);
        result = 31 * result + (int) (duracao ^ (duracao >>> 32));
        result = 31 * result + (objetivo != null ? objetivo.hashCode() : 0);
        result = 31 * result + (treinoDias != null ? treinoDias.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return getNome();
    }
}
