package com.example.ygor.minhaforca.fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.ygor.minhaforca.R;
import com.example.ygor.minhaforca.controller.dao.GenericDAO;
import com.example.ygor.minhaforca.model.GenericModel;

import java.io.Serializable;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ManterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ManterFragment<T extends GenericModel, K extends GenericDAO<T>> extends Fragment {

    private static final String ARG_RES = "res";
    private static final String ARG_MODEL = "model";
    private static final String ARG_DAO = "dao";
    private static final String ARG_MODEL_SAVER = "modelSaver";

    private int resource;
    private T model;
    private K dao;
    private ModelSaver<T, K> modelSaver;
    private View fragmentView;

    public ManterFragment() {
    }

    public static <T extends GenericModel, K extends GenericDAO<T>> ManterFragment newInstance(int resource, T model, K dao, ModelSaver<T, K> modelSaver) {
        ManterFragment fragment = new ManterFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_RES, resource);
        args.putSerializable(ARG_MODEL, model);
        args.putSerializable(ARG_DAO, dao);
        args.putSerializable(ARG_MODEL_SAVER, modelSaver);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Bundle args = getArguments();
            resource = args.getInt(ARG_RES);
            model = (T) args.getSerializable(ARG_MODEL);
            dao = (K) args.getSerializable(ARG_DAO);
            modelSaver = (ModelSaver<T, K>) args.getSerializable(ARG_MODEL_SAVER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        fragmentView = inflater.inflate(resource, container, false);

        Button button = modelSaver.onViewInflate(fragmentView, savedInstanceState, model);

        button.setOnClickListener(new onSaveButonClick());

        return fragmentView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        modelSaver.onSaveInstanceState(outState);
    }


    //    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    private class onSaveButonClick implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            long ret;
            if (modelSaver.formIsValid()) {
                modelSaver.inflateModel(model);
                if (model.getId() == -1) {
                    ret = dao.salvar(model);
                } else {
                    ret = dao.atualizar(model);
                }
                if (ret < 0) {
                    Toast.makeText(getContext(), String.valueOf(ret), Toast.LENGTH_SHORT).show();
                } else {
                    getActivity().setResult(Activity.RESULT_OK);
                    getActivity().finish();
                }
            }
        }
    }


    public interface ModelSaver<T, K> extends Serializable {
        public Button onViewInflate(View fragmentView, Bundle savedInstanceState, T t);
        public void onSaveInstanceState(Bundle outState);
        public boolean formIsValid();
        public void inflateModel(T t);
    }

}
