package com.example.ygor.minhaforca.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.ygor.minhaforca.R;

import java.lang.reflect.InvocationTargetException;

import butterknife.ButterKnife;

/**
 * Created by Ygor on 08/02/2018.
 */

public abstract class GenericAdapter<T extends GenericAdapter.GenericHolder> extends BaseAdapter {

    private LayoutInflater inflater;
    protected Context context;
    private Class<T> tClass;
    protected int resource;

    public GenericAdapter(Context context, int resource, Class<T> holderClass) {
        inflater = LayoutInflater.from(context);
        this.tClass = holderClass;
        this.resource = resource;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        T holder;

        if (view == null) {
            view = inflater.inflate(resource, parent, false);

            try {
                holder = tClass.getConstructor(this.getClass(), View.class).newInstance(this, view);
            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                Log.e(this.getClass().getName(), "Deu ruim." + e.getMessage());
                e.printStackTrace();
                return null;
            }

            view.setTag(holder);
        } else {
            holder = (T) view.getTag();
        }

        return inflateView(position, holder, view, parent);
    }

    public abstract View inflateView(int position, T holder, View view, ViewGroup parent);

    public abstract class GenericHolder {

        public View view;

        public GenericHolder(View view) {
            ButterKnife.bind(this, view);
            this.view = view;
        }

    }

}
