package com.example.ygor.minhaforca.controller.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.ygor.minhaforca.model.Exercicio;
import com.example.ygor.minhaforca.model.TreinoDia;

import java.util.List;

/**
 * Created by Ygor on 13/02/2018.
 */

public class ExercicioDAO extends GenericDAO<Exercicio> implements GenericDAO.TransformCursorListener<Exercicio> {

    public static final String NOME_TABELA = "exercicio";

    public static final String COL_ID = "_id";
    private static final String COL_NOME = "nome";
    private static final String COL_OBSERVACOES = "observacoes";
    private static final String COL_PROX_EXERC = "prox_exercicio";
    private static final String COL_ID_TREINO_DIA = "_id_treino_dia";

    public static final String SQL_TABELA = "CREATE TABLE " + NOME_TABELA + "(" +
            COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COL_NOME + " TEXT, " +
            COL_OBSERVACOES + " TEXT, " +
            COL_PROX_EXERC + " INTEGER, " +
            COL_ID_TREINO_DIA + " INTEGER, " +
            "FOREIGN KEY(" + COL_PROX_EXERC + ") REFERENCES " + NOME_TABELA + "(" + COL_ID + ") ON UPDATE CASCADE ON DELETE RESTRICT, " +
            "FOREIGN KEY(" + COL_ID_TREINO_DIA + ") REFERENCES " + TreinoDiaDAO.NOME_TABELA + "(" + TreinoDiaDAO.COL_ID + ") ON UPDATE CASCADE ON DELETE RESTRICT);";

    public ExercicioDAO(SQLiteOpenHelper helper) {
        super(helper, NOME_TABELA);
    }

    @Override
    public long salvar(Exercicio exercicio) {
        Exercicio proxExercicio = exercicio.getProxExercicio();
        ContentValues c = new ContentValues();
        c.put(COL_NOME, exercicio.getNome());
        c.put(COL_OBSERVACOES, exercicio.getObservacoes());
        c.put(COL_PROX_EXERC, (proxExercicio != null) ? proxExercicio.getId() : null);
        return this.salvar(c);
    }

    @Override
    public long atualizar(Exercicio exercicio) {
        Exercicio proxExercicio = exercicio.getProxExercicio();
        ContentValues c = new ContentValues();
        c.put(COL_NOME, exercicio.getNome());
        c.put(COL_OBSERVACOES, exercicio.getObservacoes());
        c.put(COL_PROX_EXERC, (proxExercicio != null) ? proxExercicio.getId() : null);
        return this.atualizar(COL_ID + "=?",
                new String[]{String.valueOf(exercicio.getId())},
                c);
    }

    @Override
    public long deletar(long id) {
        return this.deletar(COL_ID + "=?",
                new String[]{String.valueOf(id)});
    }

    @Override
    public Exercicio pesquisar(long id) {
        return this.pesquisar(COL_ID + "=?",
                new String[]{String.valueOf(id)},
                this);
    }

    @Override
    public List<Exercicio> listar() {
        return this.listar(this);
    }

    public List<Exercicio> listarPeloTreinoDia(TreinoDia treinoDia) {
        return this.listar(COL_ID_TREINO_DIA + "=?",
                new String[]{String.valueOf(treinoDia.getId())},
                this);
    }

    @Override
    public Exercicio getFromCursor(Cursor cursor) {
        Exercicio e = new Exercicio();
        ExercicioSerieDAO esBd = new ExercicioSerieDAO(helper);

        e.setId(cursor.getLong(cursor.getColumnIndex(COL_ID)));
        e.setNome(cursor.getString(cursor.getColumnIndex(COL_NOME)));
        e.setObservacoes(cursor.getString(cursor.getColumnIndex(COL_OBSERVACOES)));
        e.setSeries(esBd.listarPeloExercicio(e));

        e.setProxExercicio(pesquisar(cursor.getLong(cursor.getColumnIndex(COL_PROX_EXERC))));

        return e;
    }
}
