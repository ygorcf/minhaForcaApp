package com.example.ygor.minhaforca.model;

import android.app.Activity;

/**
 * Created by Ygor on 08/02/2018.
 */

public class OpcaoMain {

    public OpcaoMain() {
    }
    public OpcaoMain(String label, Class<? extends Activity> activity) {
        this.label = label;
        this.activity = activity;
    }

    private String label;
    private Class<? extends Activity> activity;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Class<? extends Activity> getActivity() {
        return activity;
    }

    public void setActivity(Class<? extends Activity> activity) {
        this.activity = activity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OpcaoMain opcaoMain = (OpcaoMain) o;

        if (label != null ? !label.equals(opcaoMain.label) : opcaoMain.label != null) return false;
        return activity != null ? activity.equals(opcaoMain.activity) : opcaoMain.activity == null;

    }

    @Override
    public int hashCode() {
        int result = label != null ? label.hashCode() : 0;
        result = 31 * result + (activity != null ? activity.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return getLabel();
    }

}
