package com.example.ygor.minhaforca.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ygor.minhaforca.R;
import com.example.ygor.minhaforca.model.Exercicio;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Ygor on 06/03/2018.
 */

public class ExerciciosAdapter extends GenericAdapter<ExerciciosAdapter.ExercicioHolder> {

    private List<Exercicio> exercicios;
    private OnItemExercicioClickListener listener;

    public ExerciciosAdapter(Context context, List<Exercicio> exercicios, OnItemExercicioClickListener onItemExercicioClickListener) {
        super(context, R.layout.item_exercicio, ExercicioHolder.class);
        this.exercicios = exercicios;
        this.listener = onItemExercicioClickListener;
    }

    @Override
    public View inflateView(int position, ExercicioHolder holder, View view, ViewGroup parent) {
        final Exercicio exercicio = (Exercicio) getItem(position);

        holder.tvNome.setText(exercicio.getNome());
        holder.view.setLongClickable(true);
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemExercicioClick(exercicio);
            }
        });

        return view;
    }

    @Override
    public int getCount() {
        return exercicios.size();
    }

    @Override
    public Object getItem(int position) {
        return exercicios.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class ExercicioHolder extends GenericAdapter.GenericHolder {

        @BindView(R.id.tvNome)
        TextView tvNome;

        public ExercicioHolder(View view) {
            super(view);
        }

    }

    public interface OnItemExercicioClickListener {
        void onItemExercicioClick(Exercicio exercicio);
    }
}
