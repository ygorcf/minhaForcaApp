package com.example.ygor.minhaforca.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;

import com.example.ygor.minhaforca.R;
import com.example.ygor.minhaforca.controller.fragmentsManter.GenericManter;
import com.example.ygor.minhaforca.controller.fragmentsManter.ManterTreino;
import com.example.ygor.minhaforca.controller.fragmentsManter.ManterTreinoDia;
import com.example.ygor.minhaforca.model.GenericModel;
import com.example.ygor.minhaforca.model.Treino;
import com.example.ygor.minhaforca.model.TreinoDia;

public class ManterActivity extends AppCompatActivity {

    private GenericManter genericManter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manter);

        if (savedInstanceState == null) {
            GenericModel modelManter = (GenericModel) getIntent().getSerializableExtra("model");

            if (modelManter instanceof Treino) {
                genericManter = new ManterTreino(this, getSupportFragmentManager(), R.id.frameFragment, R.layout.fragment_manter_treino, (Treino) modelManter);
            } else if (modelManter instanceof TreinoDia) {
                genericManter = new ManterTreinoDia(this, getSupportFragmentManager(), R.id.frameFragment, R.layout.fragment_manter_treino_dia, (TreinoDia) modelManter);
            }
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("manter", genericManter);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        genericManter = (GenericManter) savedInstanceState.getSerializable("manter");
        genericManter.setManager(getSupportFragmentManager());
    }
}
