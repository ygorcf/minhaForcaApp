package com.example.ygor.minhaforca.model;

/**
 * Created by Ygor on 10/02/2018.
 */

public class Serie extends GenericModel implements Cloneable {

    private int repeticoes;
    private long segAntes;
    private long segDepois;
    private long[] cadencia;
    private boolean picoContracao;
    private int tempos;

    public Serie() {
    }

    public Serie(int repeticoes, long segAntes, long segDepois, long[] cadencia, boolean picoContracao, int tempos) {
        this.repeticoes = repeticoes;
        this.segAntes = segAntes;
        this.segDepois = segDepois;
        this.cadencia = cadencia;
        this.picoContracao = picoContracao;
        this.tempos = tempos;
    }

    public int getRepeticoes() {
        return repeticoes;
    }

    public void setRepeticoes(int repeticoes) {
        this.repeticoes = repeticoes;
    }

    public long getSegAntes() {
        return segAntes;
    }

    public void setSegAntes(long segAntes) {
        this.segAntes = segAntes;
    }

    public long getSegDepois() {
        return segDepois;
    }

    public void setSegDepois(long segDepois) {
        this.segDepois = segDepois;
    }

    public long[] getCadencia() {
        return cadencia;
    }

    public void setCadencia(long[] cadencia) {
        this.cadencia = cadencia;
    }

    public boolean isPicoContracao() {
        return picoContracao;
    }

    public void setPicoContracao(boolean picoContracao) {
        this.picoContracao = picoContracao;
    }

    public int getTempos() {
        return tempos;
    }

    public void setTempos(int tempos) {
        this.tempos = tempos;
    }
}
