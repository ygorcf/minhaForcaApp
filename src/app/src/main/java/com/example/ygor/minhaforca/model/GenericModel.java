package com.example.ygor.minhaforca.model;

import java.io.Serializable;

/**
 * Created by Ygor on 10/02/2018.
 */

public class GenericModel implements Cloneable, Serializable {

    private long id;

    public GenericModel() {
        setId(-1);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GenericModel that = (GenericModel) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    public <T> T getClone() {
        try {
            return (T) this.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
