package com.example.ygor.minhaforca.activity;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ygor.minhaforca.R;
import com.example.ygor.minhaforca.adapter.TreinosAdapter;
import com.example.ygor.minhaforca.controller.dao.TreinoDAO;
import com.example.ygor.minhaforca.helper.DBHelper;
import com.example.ygor.minhaforca.model.Treino;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TreinosActivity extends AppCompatActivity implements TreinosAdapter.OnItemTreinoClickListener {

    private static final int CONTEXT_ALTERAR = 0;
    private static final int CONTEXT_DELETAR = 1;

    private List<Treino> treinos;
    private TreinosAdapter adapter;
    private DBHelper dbHelper;

    @BindView(R.id.listaTreinos)
    ListView listaTreinos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_treinos);
        ButterKnife.bind(this);

        dbHelper = new DBHelper(this);
        treinos = getTreinosList();

        adapter = new TreinosAdapter(this, treinos, this);
        listaTreinos.setAdapter(adapter);
        listaTreinos.setDivider(null);
        listaTreinos.setDividerHeight(0);
        registerForContextMenu(listaTreinos);

    }

    private void openManter(Treino treinoManter) {
        Intent i = new Intent(this, ManterActivity.class);
        i.putExtra("model", treinoManter);
        startActivityForResult(i, 100);
    }

    private void openDetail(Treino treinoDetail) {
        Intent i = new Intent(this, TreinoDetailActivity.class);
        i.putExtra("treino", treinoDetail);
        startActivityForResult(i, 100);
    }

    private long deletarTreino(Treino treino) {
        return new TreinoDAO(dbHelper).deletar(treino.getId());
    }

    private void updateList(List<Treino> treinosNovos) {
        treinos.clear();
        treinos.addAll(treinosNovos);
        adapter.notifyDataSetChanged();
    }

    private List<Treino> getTreinosList() {
        return new TreinoDAO(dbHelper).listar();
    }

    @OnClick(R.id.btnAdd)
    public void onBtnAddClick(View view) {
        openManter(new Treino());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        updateList(getTreinosList());
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Treino treino = (Treino) adapter.getItem(menuInfo.position);

        switch (item.getItemId()) {
            case CONTEXT_ALTERAR:
                openManter(treino);
                break;
            case CONTEXT_DELETAR:
                if (deletarTreino(treino) > 0)
                    updateList(getTreinosList());
                break;
            default:
                Toast.makeText(this, R.string.labelListarTreinoOpcaoInvalida, Toast.LENGTH_SHORT).show();
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(menu.NONE, CONTEXT_ALTERAR, menu.NONE, R.string.labelListarTreinoAlterar);
        menu.add(menu.NONE, CONTEXT_DELETAR, menu.NONE, R.string.labelListarTreinoDeletar);
    }

    @Override
    public void onItemTreinoClick(Treino treino) {
        openDetail(treino);
    }
}
