package com.example.ygor.minhaforca.model;

/**
 * Created by Ygor on 13/02/2018.
 */

public class ExercicioSerie extends GenericModel {

    private int quantidade;
    private Exercicio exercicio;
    private Serie serie;

    public ExercicioSerie() {
    }

    public ExercicioSerie(int quantidade, Exercicio exercicio, Serie serie) {
        this.quantidade = quantidade;
        this.exercicio = exercicio;
        this.serie = serie;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Exercicio getExercicio() {
        return exercicio;
    }

    public void setExercicio(Exercicio exercicio) {
        this.exercicio = exercicio;
    }

    public Serie getSerie() {
        return serie;
    }

    public void setSerie(Serie serie) {
        this.serie = serie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ExercicioSerie that = (ExercicioSerie) o;

        if (quantidade != that.quantidade) return false;
        if (exercicio != null ? !exercicio.equals(that.exercicio) : that.exercicio != null)
            return false;
        return serie != null ? serie.equals(that.serie) : that.serie == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + quantidade;
        result = 31 * result + (exercicio != null ? exercicio.hashCode() : 0);
        result = 31 * result + (serie != null ? serie.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.valueOf(getQuantidade()) + "x" + getSerie().toString();
    }
}
