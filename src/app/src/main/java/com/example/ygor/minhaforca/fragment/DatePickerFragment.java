package com.example.ygor.minhaforca.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

import java.io.Serializable;
import java.util.Calendar;

/**
 * Created by Ygor on 21/02/2018.
 */

public class DatePickerFragment extends DialogFragment {

    private static final String ARG_LISTENER = "onDateSetLis";
    private static final String ARG_DATA = "data";

    private DatePickerDialog.OnDateSetListener listener;
    private Calendar data;

    public static DatePickerFragment newInstance(DatePickerDialog.OnDateSetListener onDateSetListener, Calendar data) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_LISTENER, (Serializable) onDateSetListener);
        args.putSerializable(ARG_DATA, (Serializable) data);

        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            listener = (DatePickerDialog.OnDateSetListener) args.getSerializable(ARG_LISTENER);
            data = (Calendar) args.getSerializable(ARG_DATA);

            if (data == null) {
                data = Calendar.getInstance();
            }
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int ano = data.get(Calendar.YEAR);
        int mes = data.get(Calendar.MONTH);
        int dia = data.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getContext(), listener, ano, mes, dia);
    }
}
