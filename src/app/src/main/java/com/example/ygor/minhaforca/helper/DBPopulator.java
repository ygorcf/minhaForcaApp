package com.example.ygor.minhaforca.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.ygor.minhaforca.controller.dao.ExercicioDAO;
import com.example.ygor.minhaforca.controller.dao.ExercicioSerieDAO;
import com.example.ygor.minhaforca.controller.dao.SerieDAO;
import com.example.ygor.minhaforca.controller.dao.TreinoDAO;
import com.example.ygor.minhaforca.controller.dao.TreinoDiaDAO;
import com.example.ygor.minhaforca.model.Exercicio;
import com.example.ygor.minhaforca.model.ExercicioSerie;
import com.example.ygor.minhaforca.model.Serie;
import com.example.ygor.minhaforca.model.Treino;
import com.example.ygor.minhaforca.model.TreinoDia;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Ygor on 10/02/2018.
 */

public class DBPopulator {

    public static void populate(Context context) {

        Log.i(DBPopulator.class.getName(), "Populando o banco de dados ...");

        Log.i(DBPopulator.class.getName(), "Obtendo helper do banco ...");
        DBHelper dbHelper = new DBHelper(context);
        Log.i(DBPopulator.class.getName(), "Helper do banco obtido.");

        Log.i(DBPopulator.class.getName(), "Definindo Series ...");
        Serie s1 = new Serie();
        s1.setRepeticoes(12);
        s1.setSegAntes(-1);
        s1.setSegDepois(-1);
        s1.setCadencia(new long[]{-1, -1});
        s1.setPicoContracao(false);
        s1.setTempos(-1);

        Serie s2 = new Serie();
        s2.setRepeticoes(10);
        s2.setSegAntes(-1);
        s2.setSegDepois(-1);
        s2.setCadencia(new long[]{-1, -1});
        s2.setPicoContracao(false);
        s2.setTempos(-1);

        Serie s3 = new Serie();
        s3.setRepeticoes(15);
        s3.setSegAntes(-1);
        s3.setSegDepois(-1);
        s3.setCadencia(new long[]{-1, -1});
        s3.setPicoContracao(true);
        s3.setTempos(-1);

        Serie s4 = new Serie();
        s4.setRepeticoes(12);
        s4.setSegAntes(10);
        s4.setSegDepois(-1);
        s4.setCadencia(new long[]{-1, -1});
        s4.setPicoContracao(false);
        s4.setTempos(2);

        Serie s5 = new Serie();
        s5.setRepeticoes(10);
        s5.setSegAntes(-1);
        s5.setSegDepois(10);
        s5.setCadencia(new long[]{30, 30});
        s5.setPicoContracao(false);
        s5.setTempos(-1);

        Serie s6 = new Serie();
        s6.setRepeticoes(10);
        s6.setSegAntes(-1);
        s6.setSegDepois(-1);
        s6.setCadencia(new long[]{40, 40});
        s6.setPicoContracao(false);
        s6.setTempos(-1);

        Serie s7 = new Serie();
        s7.setRepeticoes(-1);
        s7.setSegAntes(-1);
        s7.setSegDepois(10);
        s7.setCadencia(new long[]{30, 30});
        s7.setPicoContracao(false);
        s7.setTempos(-1);

//        Serie s2 = s.getClone();
//        Serie s3 = s.getClone();
        Log.i(DBPopulator.class.getName(), "Series definidas.");

        Log.i(DBPopulator.class.getName(), "Salvando Series ...");
        SerieDAO sBd = new SerieDAO(dbHelper);

        sBd.salvar(s1);
        sBd.salvar(s2);
        sBd.salvar(s3);
        sBd.salvar(s4);
        sBd.salvar(s5);
        sBd.salvar(s6);
        sBd.salvar(s7);
        Log.i(DBPopulator.class.getName(), "Series salvas.");

        Log.i(DBPopulator.class.getName(), "Definindo ExercicioSeries ...");
        ExercicioSerie es1 = new ExercicioSerie();
        es1.setQuantidade(3);
        es1.setSerie(s1);

        ExercicioSerie es2 = new ExercicioSerie();
        es2.setQuantidade(4);
        es2.setSerie(s2);

        ExercicioSerie es3 = new ExercicioSerie();
        es3.setQuantidade(3);
        es3.setSerie(s3);

        ExercicioSerie es4 = new ExercicioSerie();
        es4.setQuantidade(2);
        es4.setSerie(s4);

        ExercicioSerie es5 = new ExercicioSerie();
        es5.setQuantidade(3);
        es5.setSerie(s5);

        ExercicioSerie es6 = new ExercicioSerie();
        es6.setQuantidade(4);
        es6.setSerie(s6);

        ExercicioSerie es7 = new ExercicioSerie();
        es7.setQuantidade(1);
        es7.setSerie(s7);

        List<ExercicioSerie> les1 = new ArrayList<>();
        les1.add(es1);

        List<ExercicioSerie> les2 = new ArrayList<>();
        les2.add(es2);

        List<ExercicioSerie> les3 = new ArrayList<>();
        les3.add(es3);

        List<ExercicioSerie> les4 = new ArrayList<>();
        les4.add(es4);

        List<ExercicioSerie> les5 = new ArrayList<>();
        les5.add(es5);

        List<ExercicioSerie> les6 = new ArrayList<>();
        les6.add(es6);
        les6.add(es7);

        Log.i(DBPopulator.class.getName(), "Definindo Exercicios ...");
        Exercicio e1 = new Exercicio();
        e1.setNome("Supino reto");
        e1.setObservacoes("");
        e1.setSeries(les1);
        e1.setProxExercicio(null);

        Exercicio e2 = new Exercicio();
        e2.setNome("Agachamento");
        e2.setObservacoes("Esse exercicio é muito bom para os quadriceps é bem top mesmo");
        e2.setSeries(les2);
        e2.setProxExercicio(null);

        Exercicio e3 = new Exercicio();
        e3.setNome("Biceps Scoth");
        e3.setObservacoes("");
        e3.setSeries(les3);
        e3.setProxExercicio(null);

        Exercicio e4 = new Exercicio();
        e4.setNome("Triceps Testa");
        e4.setObservacoes("");
        e4.setSeries(les4);
        e4.setProxExercicio(null);

        Exercicio e5 = new Exercicio();
        e5.setNome("Panturrilha sentado");
        e5.setObservacoes("");
        e5.setSeries(les5);
        e5.setProxExercicio(null);

        Exercicio e6 = new Exercicio();
        e6.setNome("Panturrilha em pe");
        e6.setObservacoes("");
        e6.setSeries(les6);
        e6.setProxExercicio(null);

        List<Exercicio> el1 = new ArrayList<>();
        el1.add(e1);

        List<Exercicio> el2 = new ArrayList<>();
        el2.add(e2);

        List<Exercicio> el3 = new ArrayList<>();
        el3.add(e3);

        List<Exercicio> el4 = new ArrayList<>();
        el4.add(e4);

        List<Exercicio> el5 = new ArrayList<>();
        el5.add(e5);
        el5.add(e6);

        List<List<Exercicio>> ell1 = new ArrayList<>();
        ell1.add(el1);

        List<List<Exercicio>> ell2 = new ArrayList<>();
        ell2.add(el2);

        List<List<Exercicio>> ell3 = new ArrayList<>();
        ell3.add(el3);

        List<List<Exercicio>> ell4 = new ArrayList<>();
        ell4.add(el4);

        List<List<Exercicio>> ell5 = new ArrayList<>();
        ell5.add(el5);

        Log.i(DBPopulator.class.getName(), "Exercicios definidos.");

        es1.setExercicio(e1);
        es2.setExercicio(e2);
        es3.setExercicio(e3);
        es4.setExercicio(e4);
        es5.setExercicio(e5);
        Log.i(DBPopulator.class.getName(), "ExercicioSeries definidas.");

        Log.i(DBPopulator.class.getName(), "Salvando exercicios ...");
        ExercicioDAO eBd = new ExercicioDAO(dbHelper);
        eBd.listar();
        eBd.salvar(e1);
        eBd.salvar(e2);
        eBd.salvar(e3);
        eBd.salvar(e4);
        eBd.salvar(e5);
        Log.i(DBPopulator.class.getName(), "Exercicios salvos.");

        Log.i(DBPopulator.class.getName(), "Salvando ExercicioSeries ...");
        ExercicioSerieDAO esBd = new ExercicioSerieDAO(dbHelper);
        esBd.salvar(es1);
        esBd.salvar(es2);
        esBd.salvar(es3);
        esBd.salvar(es4);
        esBd.salvar(es5);
        Log.i(DBPopulator.class.getName(), "ExercicioSeries salvos.");

        Log.i(DBPopulator.class.getName(), "Definindo TreinoDias ...");
        TreinoDia td1 = new TreinoDia();
        td1.setNome("A");
        td1.setDias(new int[]{TreinoDia.SEG, TreinoDia.QUA, TreinoDia.SEX});
        td1.setExercicios(ell1);

        TreinoDia td2 = new TreinoDia();
        td2.setNome("B");
        td2.setDias(new int[]{TreinoDia.TER, TreinoDia.QUI});
        td2.setExercicios(ell2);

        TreinoDia td3 = new TreinoDia();
        td3.setNome("C");
        td3.setDias(new int[]{TreinoDia.SEG, TreinoDia.QUA, TreinoDia.SEX, TreinoDia.SAB});
        td3.setExercicios(ell3);

        TreinoDia td4 = new TreinoDia();
        td4.setNome("D");
        td4.setDias(new int[]{TreinoDia.DOM});
        td4.setExercicios(ell4);

        TreinoDia td5 = new TreinoDia();
        td5.setNome("E");
        td5.setDias(new int[]{TreinoDia.SEG, TreinoDia.QUA, TreinoDia.QUI, TreinoDia.SEX});
        td5.setExercicios(ell5);

        List<TreinoDia> ltd1 = new ArrayList<>();
        ltd1.add(td1);

        List<TreinoDia> ltd2 = new ArrayList<>();
        ltd1.add(td2);

        List<TreinoDia> ltd3 = new ArrayList<>();
        ltd1.add(td3);

        List<TreinoDia> ltd4 = new ArrayList<>();
        ltd1.add(td4);

        List<TreinoDia> ltd5 = new ArrayList<>();
        ltd1.add(td5);
        Log.i(DBPopulator.class.getName(), "TreinoDias definidos.");

        Log.i(DBPopulator.class.getName(), "Salvando TreinoDias.");
        TreinoDiaDAO tdBd = new TreinoDiaDAO(dbHelper);
        tdBd.salvar(td1);
        tdBd.salvar(td2);
        tdBd.salvar(td3);
        tdBd.salvar(td4);
        tdBd.salvar(td5);
        Log.i(DBPopulator.class.getName(), "TreinoDias salvos.");

        Log.i(DBPopulator.class.getName(), "Definindo Treinos.");
        Calendar c1 = new GregorianCalendar();
        c1.set(2018, 2, 16);
        Calendar c2 = new GregorianCalendar();
        c2.set(2018, 2, 20);
        Calendar c3 = new GregorianCalendar();
        c3.set(2018, 3, 22);
        Calendar c4 = new GregorianCalendar();
        c4.set(2018, 2, 17);
        Calendar c5 = new GregorianCalendar();
        c5.set(2018, 3, 19);

        Treino t1 = new Treino();
        t1.setNome("Treino Capitão America");
        t1.setDuracao(c1.getTimeInMillis());
        t1.setObjetivo("Hipertrofia");
        t1.setTreinoDias(ltd1);

        Treino t2 = new Treino();
        t2.setNome("Treino TOP");
        t2.setDuracao(c2.getTimeInMillis());
        t2.setObjetivo("Hipertrofia");
        t2.setTreinoDias(ltd2);

        Treino t3 = new Treino();
        t3.setNome("Treino para magrelos");
        t3.setDuracao(c3.getTimeInMillis());
        t3.setObjetivo("Hipertrofia");
        t3.setTreinoDias(ltd3);

        Treino t4 = new Treino();
        t4.setNome("Treino THOR");
        t4.setDuracao(c4.getTimeInMillis());
        t4.setObjetivo("Hipertrofia");
        t4.setTreinoDias(ltd4);

        Treino t5 = new Treino();
        t5.setNome("Treino Jacaré");
        t5.setDuracao(c5.getTimeInMillis());
        t5.setObjetivo("Hipertrofia");
        t5.setTreinoDias(ltd5);
        Log.i(DBPopulator.class.getName(), "Treinos definidos.");

        Log.i(DBPopulator.class.getName(), "Salvando Treinos.");
        TreinoDAO tBd = new TreinoDAO(dbHelper);
        tBd.salvar(t1);
        tBd.salvar(t2);
        tBd.salvar(t3);
        tBd.salvar(t4);
        tBd.salvar(t5);
        Log.i(DBPopulator.class.getName(), "Treinos salvos.");

        Log.i(DBPopulator.class.getName(), "BANCO DE DADOS POPULADO COM SUCESSO.");

    }

    public static void deleteAll(SQLiteDatabase db) {
        Log.i(DBPopulator.class.getName(), "Limpando Banco de dados ...");
        db.execSQL("DELETE FROM " + SerieDAO.NOME_TABELA + ";");
        db.execSQL("DELETE FROM " + ExercicioDAO.NOME_TABELA + ";");
        db.execSQL("DELETE FROM " + ExercicioSerieDAO.NOME_TABELA + ";");
        db.execSQL("DELETE FROM " + TreinoDiaDAO.NOME_TABELA + ";");
        db.execSQL("DELETE FROM " + TreinoDAO.NOME_TABELA + ";");
        Log.i(DBPopulator.class.getName(), "BANCO DE DADOS LIMPO COM SUCESSO.");
    }

}
