package com.example.ygor.minhaforca.controller.fragmentsManter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.res.TypedArrayUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.ygor.minhaforca.R;
import com.example.ygor.minhaforca.controller.dao.TreinoDiaDAO;
import com.example.ygor.minhaforca.helper.DBHelper;
import com.example.ygor.minhaforca.model.TreinoDia;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ygor on 04/03/2018.
 */

public class ManterTreinoDia extends GenericManter<TreinoDia, TreinoDiaDAO> {

    @BindView(R.id.btnSave)
    Button btnSave;

    @BindView(R.id.campoManterTreinoDiaNome)
    EditText campoNome;

    @BindView(R.id.cbManterTreinoDiaDom)
    CheckBox cbDom;

    @BindView(R.id.cbManterTreinoDiaSeg)
    CheckBox cbSeg;

    @BindView(R.id.cbManterTreinoDiaTer)
    CheckBox cbTer;

    @BindView(R.id.cbManterTreinoDiaQua)
    CheckBox cbQua;

    @BindView(R.id.cbManterTreinoDiaQui)
    CheckBox cbQui;

    @BindView(R.id.cbManterTreinoDiaSex)
    CheckBox cbSex;

    @BindView(R.id.cbManterTreinoDiaSab)
    CheckBox cbSab;

    public ManterTreinoDia(Context context, FragmentManager manager, int frameLayout, int fragmentLayout, TreinoDia treinoDia) {
        super(manager, frameLayout, fragmentLayout, treinoDia, new TreinoDiaDAO(new DBHelper(context)));
    }

    private void clearDias() {
        cbDom.setChecked(false);
        cbSeg.setChecked(false);
        cbTer.setChecked(false);
        cbQua.setChecked(false);
        cbQui.setChecked(false);
        cbSex.setChecked(false);
        cbSab.setChecked(false);
    }

    private void setDia(int dia, boolean checked) {
        switch (dia) {
            case TreinoDia.DOM:
                cbDom.setChecked(checked);
                break;
            case TreinoDia.SEG:
                cbSeg.setChecked(checked);
                break;
            case TreinoDia.TER:
                cbTer.setChecked(checked);
                break;
            case TreinoDia.QUA:
                cbQua.setChecked(checked);
                break;
            case TreinoDia.QUI:
                cbQui.setChecked(checked);
                break;
            case TreinoDia.SEX:
                cbSex.setChecked(checked);
                break;
            case TreinoDia.SAB:
                cbSab.setChecked(checked);
                break;
        }
    }

    private void setDias(int[] dias) {
        for (int i = 0, ii = dias.length; i < ii; i++) {
            setDia(dias[i], true);
        }
    }

    private int[] getDias() {
        List<Integer> dias = new ArrayList<>();

        if (cbDom.isChecked())
            dias.add(TreinoDia.DOM);
        if (cbSeg.isChecked())
            dias.add(TreinoDia.SEG);
        if (cbTer.isChecked())
            dias.add(TreinoDia.TER);
        if (cbQua.isChecked())
            dias.add(TreinoDia.QUA);
        if (cbQui.isChecked())
            dias.add(TreinoDia.QUI);
        if (cbSex.isChecked())
            dias.add(TreinoDia.SEX);
        if (cbSab.isChecked())
            dias.add(TreinoDia.SAB);

        int[] ret = new int[dias.size()];

        for (int i = 0, ii = dias.size(); i < ii; i++) {
            ret[i] = dias.get(i).intValue();
        }

        return ret;
    }

    @Override
    public Button onViewInflate(View fragmentView, Bundle savedInstanceState, TreinoDia treinoDia) {
        ButterKnife.bind(this, fragmentView);

        int[] diasCheckeds = new int[0];

        if (treinoDia.getId() != -1) {
            campoNome.setText(treinoDia.getNome());
            diasCheckeds = treinoDia.getDias();
        }
        if (savedInstanceState != null) {
            diasCheckeds = savedInstanceState.getIntArray("dias");
            campoNome.setText(savedInstanceState.getString("nome"));
        }
        clearDias();
        setDias(diasCheckeds);

        return btnSave;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("nome", campoNome.getText().toString());
        outState.putIntArray("dias", getDias());
    }

    @Override
    public boolean formIsValid() {
        if (campoNome.length() == 0) {
            return false;
        }
        if (getDias().length == 0) {
            return false;
        }
        return true;
    }

    @Override
    public void inflateModel(TreinoDia treinoDia) {
        treinoDia.setNome(campoNome.getText().toString());
        treinoDia.setDias(getDias());
    }
}
