package com.example.ygor.minhaforca.model;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Ygor on 09/02/2018.
 */

public class TreinoDia extends GenericModel {

    public static final int DOM = 0;
    public static final int SEG = 1;
    public static final int TER = 2;
    public static final int QUA = 3;
    public static final int QUI = 4;
    public static final int SEX = 5;
    public static final int SAB = 6;

    private List<List<Exercicio>> exercicios;
    private int[] dias;
    private String nome;

    public TreinoDia() {
    }

    public TreinoDia(List<List<Exercicio>> exercicios, int[] dias, String nome) {
        this.exercicios = exercicios;
        this.dias = dias;
        this.nome = nome;
    }

    public List<List<Exercicio>> getExercicios() {
        return exercicios;
    }

    public void setExercicios(List<List<Exercicio>> exercicios) {
        this.exercicios = exercicios;
    }

    public int[] getDias() {
        return dias;
    }

    public String getDiasStr() {
        return getDiasStr(",");
    }

    public String getDiasStr(String separator) {
        StringBuffer ret = new StringBuffer();
        int[] dias = getDias();
        for (int i = 0; i < dias.length; i++) {
            if (i > 0) {
                ret.append(separator);
            }
            ret.append(dias[i]);
        }
        return ret.toString();
    }

    public void setDias(int[] dias) {
        this.dias = dias;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TreinoDia treinoDia = (TreinoDia) o;

        if (exercicios != null ? !exercicios.equals(treinoDia.exercicios) : treinoDia.exercicios != null)
            return false;
        if (!Arrays.equals(dias, treinoDia.dias)) return false;
        return nome != null ? nome.equals(treinoDia.nome) : treinoDia.nome == null;

    }

    @Override
    public int hashCode() {
        int result = exercicios != null ? exercicios.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(dias);
        result = 31 * result + (nome != null ? nome.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return getNome();
    }
}
