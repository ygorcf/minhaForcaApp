package com.example.ygor.minhaforca.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ygor.minhaforca.R;
import com.example.ygor.minhaforca.adapter.TreinoDiaAdapter;
import com.example.ygor.minhaforca.controller.dao.TreinoDiaDAO;
import com.example.ygor.minhaforca.helper.DBHelper;
import com.example.ygor.minhaforca.helper.DateHelper;
import com.example.ygor.minhaforca.model.Treino;
import com.example.ygor.minhaforca.model.TreinoDia;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TreinoDetailActivity extends AppCompatActivity implements TreinoDiaAdapter.OnItemTreinoDiaClickListener {

    private final int CONTEXT_ALTERAR = 0;
    private final int CONTEXT_DELETAR = 1;

    private List<TreinoDia> treinoDias;
    private DBHelper helper;
    private TreinoDiaAdapter adapter;
    private Treino treino;

    @BindView(R.id.tvObjetivo)
    TextView tvObjetivo;

    @BindView(R.id.tvDuracao)
    TextView tvDuracao;

    @BindView(R.id.listaTreinoDias)
    ListView listaTreinoDias;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_treino_detail);
        ButterKnife.bind(this);

        treino = (Treino) getIntent().getSerializableExtra("treino");

        if (treino == null) {
            Toast.makeText(this, "Ocorreu um erro ao obter os dados do treino", Toast.LENGTH_SHORT).show();
            finish();
        }

        setTitle(treino.getNome());
        tvObjetivo.setText(treino.getObjetivo());
        tvDuracao.setText(DateHelper.formatDate(treino.getDuracao()));

        helper = new DBHelper(this);

        treinoDias = getTreinoDiasList();

        adapter = new TreinoDiaAdapter(this, treinoDias, this);

        listaTreinoDias.setAdapter(adapter);
        listaTreinoDias.setDivider(null);
        listaTreinoDias.setDividerHeight(0);
        registerForContextMenu(listaTreinoDias);

    }

    private void openManterTreinoDia(TreinoDia treinoDia) {
        Intent i = new Intent(this, ManterActivity.class);
        i.putExtra("model", treinoDia);
        startActivityForResult(i, 100);
    }

    private void openDetail(TreinoDia treinoDia) {
        Intent i = new Intent(this, TreinoDiaDetailActivity.class);
        i.putExtra("treinoDia", treinoDia);
        startActivityForResult(i, 100);
    }

    private long deletarTreinoDia(TreinoDia treinoDia) {
        return new TreinoDiaDAO(helper).deletar(treinoDia.getId());
    }

    private void updateList(List<TreinoDia> treinoDiasNovos) {
        treinoDias.clear();
        treinoDias.addAll(treinoDiasNovos);
        adapter.notifyDataSetChanged();
    }

    private List<TreinoDia> getTreinoDiasList() {
//        return new TreinoDiaDAO(helper).listarPeloTreino(treino);
        return new TreinoDiaDAO(helper).listar();
    }

    @OnClick(R.id.btnAdd)
    public void onBtnAddClick() {
        openManterTreinoDia(new TreinoDia());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        updateList(getTreinoDiasList());
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        TreinoDia treinoDia = (TreinoDia) adapter.getItem(info.position);

        switch (item.getItemId()) {
            case CONTEXT_ALTERAR:
                openManterTreinoDia(treinoDia);
                break;
            case CONTEXT_DELETAR:
                if (deletarTreinoDia(treinoDia) > 0)
                    updateList(getTreinoDiasList());
                break;
            default:
                Toast.makeText(this, R.string.labelTreinoDetailOpcaoInvalida, Toast.LENGTH_SHORT).show();
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(menu.NONE, CONTEXT_ALTERAR, menu.NONE, R.string.labelTreinoDetailAlterar);
        menu.add(menu.NONE, CONTEXT_DELETAR, menu.NONE, R.string.labelTreinoDetailDeletar);
    }

    @Override
    public void onItemTreinoDiaClick(TreinoDia treinoDia) {
        openDetail(treinoDia);
    }

}
