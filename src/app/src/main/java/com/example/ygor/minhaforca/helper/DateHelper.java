package com.example.ygor.minhaforca.helper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Ygor on 28/02/2018.
 */

public class DateHelper {

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public static String formatDate(Date date) {
        return dateFormat.format(date);
    }

    public static String formatDate(Calendar calendar) {
        return formatDate(calendar.getTime());
    }

    public static String formatDate(long timeMillis) {
        Calendar c = Calendar.getInstance();
        c.clear();
        c.setTimeInMillis(timeMillis);
        return formatDate(c);
    }

}
