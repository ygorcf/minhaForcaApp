package com.example.ygor.minhaforca.adapter;

import android.content.Context;
import android.view.ContextMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ygor.minhaforca.R;
import com.example.ygor.minhaforca.model.Treino;

import java.util.List;

import butterknife.BindView;
import butterknife.OnLongClick;

/**
 * Created by Ygor on 17/02/2018.
 */

public class TreinosAdapter extends GenericAdapter<TreinosAdapter.TreinosHolder> {

    private List<Treino> treinos;
    private OnItemTreinoClickListener itemClickListener;

    public TreinosAdapter(Context context, List<Treino> treinos, OnItemTreinoClickListener itemClickListener) {
        super(context, R.layout.item_treino, TreinosHolder.class);
        this.treinos = treinos;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public View inflateView(final int position, TreinosHolder holder, View view, ViewGroup parent) {
        final Treino treino = (Treino) getItem(position);

        holder.tvNome.setText(treino.getNome());
        holder.view.setLongClickable(true);
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemTreinoClick(treino);
            }
        });

        return view;
    }

    @Override
    public int getCount() {
        return treinos.size();
    }

    @Override
    public Object getItem(int position) {
        return treinos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class TreinosHolder extends GenericAdapter.GenericHolder {

        @BindView(R.id.tvNome)
        TextView tvNome;

        public TreinosHolder(View view) {
            super(view);
        }
    }

    public interface OnItemTreinoClickListener {
        void onItemTreinoClick(Treino treino);
    }

}
