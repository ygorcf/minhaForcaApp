package com.example.ygor.minhaforca.controller.dao;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.ygor.minhaforca.model.Exercicio;
import com.example.ygor.minhaforca.model.Treino;
import com.example.ygor.minhaforca.model.TreinoDia;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Ygor on 13/02/2018.
 */

public class TreinoDiaDAO extends GenericDAO<TreinoDia> implements GenericDAO.TransformCursorListener<TreinoDia> {

    public static final String NOME_TABELA = "treino_dia";

    public static final String COL_ID = "_id";
    private static final String COL_NOME = "nome";
    private static final String COL_DIAS = "dias";
    private static final String COL_ID_TREINO = "_id_treino";

    public static final String SQL_TABELA = "CREATE TABLE " + NOME_TABELA + "(" +
            COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COL_NOME + " TEXT, " +
            COL_DIAS + " TEXT, " +
            COL_ID_TREINO + " INTEGER, " +
            "FOREIGN KEY(" + COL_ID_TREINO + ") REFERENCES " + TreinoDAO.NOME_TABELA + "(" + TreinoDAO.COL_ID + ") ON UPDATE CASCADE ON DELETE RESTRICT);";

    public TreinoDiaDAO(SQLiteOpenHelper helper) {
        super(helper, NOME_TABELA);
    }

    @Override
    public long salvar(TreinoDia treinoDia) {
        ContentValues c = new ContentValues();
        c.put(COL_NOME, treinoDia.getNome());
        c.put(COL_DIAS, treinoDia.getDiasStr());
        return this.salvar(c);
    }

    @Override
    public long atualizar(TreinoDia treinoDia) {
        ContentValues c = new ContentValues();
        c.put(COL_NOME, treinoDia.getNome());
        c.put(COL_DIAS, treinoDia.getDiasStr());
        return this.atualizar(COL_ID + "=?",
                new String[]{String.valueOf(treinoDia.getId())},
                c);
    }

    @Override
    public long deletar(long id) {
        return this.deletar(COL_ID + "=?",
                new String[]{String.valueOf(id)});
    }

    @Override
    public TreinoDia pesquisar(long id) {
        return pesquisar(COL_ID + "=?",
                new String[]{String.valueOf(id)},
                this);
    }

    @Override
    public List<TreinoDia> listar() {
        return listar(this);
    }


    public List<TreinoDia> listarPeloTreino(Treino treino) {
        return listar(COL_ID_TREINO + "=?",
                        new String[]{String.valueOf(treino.getId())},
                        this);
    }

    private List<List<Exercicio>> getListaCompletaExercicios(TreinoDia td) {
        ExercicioDAO eBd = new ExercicioDAO(helper);
        List<Exercicio> exercicios = eBd.listarPeloTreinoDia(td);
        List<List<Exercicio>> ret = new ArrayList<>();

        for (Exercicio e: exercicios) {
            ret.add(e.getListExercicios());
        }

        return ret;
    }

    @Override
    public TreinoDia getFromCursor(Cursor cursor) {
        TreinoDia td = new TreinoDia();
        String[] diasStr = cursor.getString(cursor.getColumnIndex(COL_DIAS)).split(",");
        int[] dias = new int[diasStr.length];

        for (int i = 0; i < diasStr.length; i++) {
            dias[i] = Integer.parseInt(diasStr[i]);
        }

        td.setId(cursor.getLong(cursor.getColumnIndex(COL_ID)));
        td.setNome(cursor.getString(cursor.getColumnIndex(COL_NOME)));
        td.setDias(dias);
        td.setExercicios(getListaCompletaExercicios(td));

        return td;
    }
}
