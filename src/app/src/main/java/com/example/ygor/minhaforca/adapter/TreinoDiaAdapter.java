package com.example.ygor.minhaforca.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ygor.minhaforca.R;
import com.example.ygor.minhaforca.model.TreinoDia;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Ygor on 28/02/2018.
 */

public class TreinoDiaAdapter extends GenericAdapter<TreinoDiaAdapter.TreinoDiaHolder> {

    private List<TreinoDia> treinoDias;
    private OnItemTreinoDiaClickListener onItemTreinoDiaClickListener;

    public TreinoDiaAdapter(Context context, List<TreinoDia> treinoDias, OnItemTreinoDiaClickListener onItemTreinoDiaClickListener) {
        super(context, R.layout.item_treino_dia, TreinoDiaHolder.class);
        this.treinoDias = treinoDias;
        this.onItemTreinoDiaClickListener = onItemTreinoDiaClickListener;
    }

    @Override
    public View inflateView(int position, TreinoDiaHolder holder, View view, ViewGroup parent) {
        final TreinoDia treinoDia = (TreinoDia) getItem(position);

        holder.tvNome.setText(treinoDia.getNome());
        holder.view.setLongClickable(true);
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemTreinoDiaClickListener.onItemTreinoDiaClick(treinoDia);
            }
        });

        return view;
    }

    @Override
    public int getCount() {
        return treinoDias.size();
    }

    @Override
    public Object getItem(int position) {
        return treinoDias.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class TreinoDiaHolder extends GenericAdapter.GenericHolder {

        @BindView(R.id.tvNome)
        TextView tvNome;

        public TreinoDiaHolder(View view) {
            super(view);
        }
    }

    public interface OnItemTreinoDiaClickListener {
        void onItemTreinoDiaClick(TreinoDia treinoDia);
    }
}
